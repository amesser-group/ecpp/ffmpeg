#! /usr/bin/env python
# vim: set fileencoding=utf-8 ts=4 sw=4 et

def configure(conf):
    f = conf.env.append_value
    f('INCLUDES',  [ 
        conf.path.find_dir('include').abspath(),
        conf.path.find_dir('ffmpeg').abspath(),
    ])

def build(bld):
    bld.ecpp_build(
        target   = 'ecpp-ffmpeg',
        source   = [
            "ffmpeg/libavcodec/mpegaudiodec_common.c",
            "ffmpeg/libavcodec/mpegaudiodec_fixed.c",
            "ffmpeg/libavcodec/mpegaudiodecheader.c",
            "ffmpeg/libavcodec/mpegaudiodsp.c",
            "ffmpeg/libavcodec/mpegaudiodsp_data.c",
            "ffmpeg/libavcodec/mpegaudiodsp_fixed.c",
        ],
        includes = [
            "src"
        ],
        features = 'cxx cstlib',
    )
