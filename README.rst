#######################
ECPP ffmpeg Integration
#######################

This module integrates a subset of the [ffmpeg]_ libraries into the 
ecpp platform.

Copyright & License
===================

See ``ffmpeg`` subfolder for license information related to ffmpeg. 


References
=========

.. [ffmpeg] https://www.ffmpeg.org/

