/*
 *  Copyright 2022 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the ecpp ffmpeg integration
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ECPP_FFMPEG_CONFIG_H_
#define ECPP_FFMPEG_CONFIG_H_

#define HAVE_CBRT     1
#define HAVE_CBRTF    1
#define HAVE_COPYSIGN 1
#define HAVE_ERF      1
#define HAVE_HYPOT    1
#define HAVE_ISNAN    1
#define HAVE_ISFINITE 1
#define HAVE_RINT     1
#define HAVE_LRINT    1
#define HAVE_LRINTF   1
#define HAVE_ROUND    1
#define HAVE_ROUNDF   1
#define HAVE_TRUNC    1
#define HAVE_TRUNCF   1

#define ARCH_ARM      1
#define ARCH_AARCH64  0
#define ARCH_PPC      0
#define ARCH_X86      0
#define HAVE_MIPSFPU  0
#define HAVE_MIPSDSP  0

#define av_restrict __restrict__


#define CONFIG_MP3_DECODER 1
#endif